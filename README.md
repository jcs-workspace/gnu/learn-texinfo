# learn-texinfo

Sample code by following the tutorial [GNU texinfo ~ Beautiful manuals and info pages ~ (Part 1 - Introduction and setup)](https://www.youtube.com/watch?v=k9FQ-1REip0).

```
$ makeinfo kop.texi
```

or for HTML:

```
$ makeinfo kop.texi --html
```

*P.S. For some reasons, Windows `makeinfo` doesn't generate HTML page!
I will still need to investigate it!*
